from django.shortcuts import render

from rest_framework import viewsets

from models import *
from serializers import *

"""
    App : - Tatkal
"""

class CategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SubCategory to be viewed or edited.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class OffersViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SubSubCategory to be viewed or edited.
    """
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer
