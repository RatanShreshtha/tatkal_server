from django.shortcuts import render

from rest_framework import filters
from rest_framework import viewsets

from models import *
from serializers import *

"""
    App : - Grocery
"""


class SubCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SubCategory to be viewed or edited.
    """
    queryset = SubCategory.objects.all()
    serializer_class = SubCategorySerializer


class SubSubCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows SubSubCategory to be viewed or edited.
    """
    queryset = SubSubCategory.objects.all()
    serializer_class = SubSubCategorySerializer
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
    ordering_fields = ['name']
    filter_fields = ['subcategory__name',]


class ProductVarientViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows ProductVarient to be viewed or edited.
    """
    queryset = ProductVarient.objects.all()
    serializer_class = ProductVarientSerializer


class ProductDetailViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows ProductDetail to be viewed or edited.
    """
    queryset = ProductDetail.objects.all()
    serializer_class = ProductDetailSerializer
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
    ordering_fields = ['name', 'company']
    filter_fields = ['company',
                     'subcategory__name',
                     'subsubcategory__name',
                     'varient__size',
                     'varient__price']
