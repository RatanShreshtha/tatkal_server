from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from babycare import views
# Registering URLs For Baby Care

router = DefaultRouter()


router.register(r'subcategories', views.SubCategoryViewSet)
router.register(r'subsubcategories', views.SubSubCategoryViewSet)
router.register(r'productVarients', views.ProductVarientViewSet)
router.register(r'products', views.ProductDetailViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
