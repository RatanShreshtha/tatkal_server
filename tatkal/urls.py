from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from tatkal import views

# Registering URLs For Tatkal

router = DefaultRouter()


router.register(r'categories', views.CategoryViewSet)
router.register(r'offers', views.OffersViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
]
