from django.contrib import admin

from .models import *

admin.site.register(SubCategory)
admin.site.register(SubSubCategory)
admin.site.register(ProductVarient)
admin.site.register(ProductDetail)
