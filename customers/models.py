from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver
from django.shortcuts import redirect
from django.utils.encoding import force_text

from allauth.account.models import EmailAddress
from allauth.account.signals import user_signed_up
from allauth.account.utils import perform_login
from allauth.exceptions import ImmediateHttpResponse
from allauth.socialaccount.signals import pre_social_login

from phonenumber_field.modelfields import PhoneNumberField


class CustomerProfile(models.Model):
    user = models.ForeignKey(User)
    avatar_url = models.CharField(max_length=256, blank=True, null=True)
    dob = models.DateField('Date Of Birth', blank=True, null=True)
    phone_num = PhoneNumberField("Contact Number", blank=True, null=True)
    address = models.TextField('Address', blank=True)
    is_complete = models.BooleanField('Profile Complete', default=False)

    class Meta:
        verbose_name = "CustomerProfile"
        verbose_name_plural = "CustomerProfiles"

    def account_verified(self):
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email=self.user.email)
            if len(result):
                return result[0].verified
        return False

    def __unicode__(self):
        return force_text(self.user.email)


class OrderItem(models.Model):
    name = models.CharField('Name Of Item', max_length=256)
    image = models.CharField('Image Of Item', max_length=300)
    price = models.CharField('Price Of Item', max_length=15)
    quantity = models.CharField('Quantity Of Item', max_length=10)
    unit = models.CharField('Unit', max_length=256)
    category = models.CharField('Category', max_length=256)
    subcategory = models.CharField('Sub-Category', max_length=256)
    subsubcategory = models.CharField('Sub-Sub-Category', max_length=256)

    class Meta:
        verbose_name = "OrderItem"
        verbose_name_plural = "OrderItems"

    def __unicode__(self):
        return self.name


class CustomerOrder(models.Model):
    orderid = models.CharField(max_length=50, null=True, blank=True, unique=True)
    user = models.ForeignKey(User)
    items = models.ManyToManyField(OrderItem)
    totalamount = models.IntegerField()

    class Meta:
        verbose_name = "CustomerOrder"
        verbose_name_plural = "CustomerOrders"

    def __init__(self):
         super(CustomerOrder, self).__init__()
         self.orderid = str(uuid.uuid4())

    def __unicode__(self):
        return self.orderid


@receiver(user_signed_up)
def set_initial_user_names(request, user, sociallogin=None, **kwargs):

    preferred_avatar_size_pixels = 256

    picture_url = "http://www.gravatar.com/avatar/{0}?s={1}".format(
        hashlib.md5(user.email.encode('UTF-8')).hexdigest(),
        preferred_avatar_size_pixels
    )

    if sociallogin:

        if sociallogin.account.provider == 'facebook':
            user.first_name = sociallogin.account.extra_data['first_name']
            user.last_name = sociallogin.account.extra_data['last_name']
            picture_url = "http://graph.facebook.com/{0}/picture?width={1}&height={1}".format(
                sociallogin.account.uid, preferred_avatar_size_pixels)

        if sociallogin.account.provider == 'google':
            user.first_name = sociallogin.account.extra_data['given_name']
            user.last_name = sociallogin.account.extra_data['family_name']
            picture_url = sociallogin.account.extra_data['picture']

    profile = UserProfile(user=user, avatar_url=picture_url)
    profile.save()

    user.guess_display_name()
    user.save()


@receiver(pre_social_login)
def link_to_local_user(sender, request, sociallogin, **kwargs):
    email_address = sociallogin.account.extra_data['email']
    users = User.objects.filter(email=email_address)
    if users:
        perform_login(request, users[0], email_verification='optional')
        raise ImmediateHttpResponse(redirect('/'))
