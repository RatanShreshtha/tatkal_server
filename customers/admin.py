from django.contrib import admin

from .models import *

admin.site.register(CustomerProfile)
admin.site.register(OrderItem)
admin.site.register(CustomerOrder)
