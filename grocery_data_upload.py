#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tatkal_server.settings")

    from django.core.wsgi import get_wsgi_application

    application = get_wsgi_application()


import csv
from collections import defaultdict

import django
from django.core.files import File

from grocery.models import *

dataFile = open('CSV/Grocery.csv')
dataFileReader = csv.reader(dataFile)
data = list(dataFileReader)

l = len(data)

d = defaultdict(list)
s = []

for i in xrange(1, l):
    s.append((data[i][3], data[i][4]))

for k, v in s:
    d[k].append(v)

for j in d:
    temp = set(d[j])
    d[j] = temp

for subcat in d.keys():
    print subcat
    subCatObject = SubCategory(name=subcat)
    # sub_cat_img_path = '../babycareImages/' + subcat + '.jpg'
    sub_cat_img_path = '/home/ratan/Pictures/sc.png'

    subCatObject.image.save(subcat, File(open(sub_cat_img_path, 'r')))

    for ssubcat in d[subcat]:
        print '\t--', ssubcat
        subCatObject.subsubcategory_set.create(name=ssubcat)
    print '-' * 30

print
print '-' * 25, 'Products', '-' * 25
print

for i in xrange(1, l):
    name = data[i][0]
    company = data[i][1]
    cat = data[i][2]
    sub_cat = data[i][3]
    sub_sub_cat = data[i][4]
    varients = data[i][5].split()

    print i, '\t', name

    varients_list = []
    for var in varients:
        size, price = var.split('/')
        varients_list.append([size, price])

    productObj = ProductDetail()
    productObj.name = name
    productObj.company = company
    productObj.description = 'Killu'

    # product_img_path = '../babycareImages/' + sub_cat + '/' + sub_sub_cat + '/' + name + '.jpg'
    product_img_path = '/home/ratan/Pictures/sc.png'

    subCatObj = SubCategory.objects.get(name=sub_cat)
    subSubCatObj = SubSubCategory.objects.get(name=sub_sub_cat)
    subSubCatObj.subcategory = subCatObj

    productObj.subcategory = subCatObj
    productObj.subsubcategory = subSubCatObj
    productObj.image.save(name, File(open(product_img_path, 'rb')))

    for varient in varients_list:
        varObj = ProductVarient(size=varient[0], price=varient[1])
        varObj.save()

        productObj.varient.add(varObj)
