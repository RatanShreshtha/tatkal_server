from __future__ import unicode_literals

from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(default="Category")
    image = models.ImageField(upload_to='categories/')

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

    def __unicode__(self):
        return self.name


class Offer(models.Model):
    name = models.CharField(max_length=50)
    offercode = models.CharField(max_length=50)
    description = models.TextField(default="Offer For You.")
    image = models.ImageField(upload_to='offers/')

    class Meta:
        verbose_name = "Offer"
        verbose_name_plural = "Offers"

    def __unicode__(self):
        return self.offercode
