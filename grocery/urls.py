from django.conf.urls import url, include

from rest_framework.routers import DefaultRouter

from grocery import views as groceryViews

# Registering URLs For Grocery

router = DefaultRouter()

router.register(r'subcategories', groceryViews.SubCategoryViewSet)
router.register(r'subsubcategories', groceryViews.SubSubCategoryViewSet)
router.register(r'productVarients', groceryViews.ProductVarientViewSet)
router.register(r'products', groceryViews.ProductDetailViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
