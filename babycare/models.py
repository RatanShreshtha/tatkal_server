from __future__ import unicode_literals

from django.db import models


class SubCategory(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(default="SubCategory")
    image = models.ImageField(upload_to='babycare/subcategory/')

    class Meta:
        verbose_name = "SubCategory"
        verbose_name_plural = "SubCategories"

    def __unicode__(self):
        return self.name


class SubSubCategory(models.Model):
    name = models.CharField(max_length=50)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "SubSubCategory"
        verbose_name_plural = "SubSubCategories"

    def __unicode__(self):
        return self.name


class ProductVarient(models.Model):
    size = models.CharField(max_length=50)
    price = models.CharField(max_length=10)

    class Meta:
        verbose_name = "ProductVarient"
        verbose_name_plural = "ProductVarients"

    def __unicode__(self):
        return self.size + '/' + self.price


class ProductDetail(models.Model):
    name = models.CharField(max_length=150)
    company = models.CharField(max_length=150)
    image = models.ImageField(upload_to='babycare/products/')
    description = models.TextField(null=True)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    subsubcategory = models.ForeignKey(SubSubCategory, on_delete=models.CASCADE)
    varient = models.ManyToManyField(ProductVarient)

    class Meta:
        verbose_name = "ProductDetail"
        verbose_name_plural = "ProductDetails"

    def __unicode__(self):
        return self.name
