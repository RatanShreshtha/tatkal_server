import os
from PIL import Image

rootPath = raw_input('Enter The Path Of Sourse Directory:-')
destDir = raw_input('Enter The Path Of Destination Directory:-')


count = 1

print '-' * 30, 'Images', '-' * 30

for root, directories, filenames in os.walk(rootPath):
    for filename in filenames:
        try:
            foo = Image.open(os.path.join(root, filename))
            foo = foo.resize((310, 310), Image.ANTIALIAS)
	    fname = filename.replace('jpg', '') + 'png'
            foo.save(os.path.join(destDir, fname), optimize=True, quality=90)
	    print count, '\t', filename
            count += 1
        except Exception as e:
            continue
