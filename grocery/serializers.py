from rest_framework import serializers

from models import *


class SubCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = SubCategory
        fields = '__all__'


class SubSubCategorySerializer(serializers.ModelSerializer):
    subcategory = SubCategorySerializer()

    class Meta:
        model = SubSubCategory
        fields = '__all__'


class ProductVarientSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductVarient
        fields = '__all__'


class ProductDetailSerializer(serializers.ModelSerializer):
    subcategory = SubCategorySerializer()
    subsubcategory = SubSubCategorySerializer()
    varient = ProductVarientSerializer(many=True)

    class Meta:
        model = ProductDetail
        fields = '__all__'
