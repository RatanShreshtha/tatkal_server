import datetime

from haystack import indexes
from models import SubCategory, SubSubCategory, ProductDetail


class SubCategoryIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')
    description = indexes.CharField(model_attr='description')


    def get_model(self):
        return SubCategory


# class SubSubCategoryIndex(indexes.SearchIndex, indexes.Indexable):
#     text = indexes.CharField(document=True, use_template=True)
#     name = indexes.CharField(model_attr='name')
#
#
#     def get_model(self):
#         return SubSubCategory


class ProductDetailIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name')
    company = indexes.CharField(model_attr='company')
    description = indexes.CharField(model_attr='description')


    def get_model(self):
        return ProductDetail
